public class Lesson1 {
    public static void main(String[] args) {
        // First option
        int hex = 16;
        int n = 3;
        int A = 10;
        int C = 12;
        int D = 13;
        double dec = (A * (Math.pow(hex, n))) + (C * (Math.pow(hex, --n))) + (D * (Math.pow(hex, --n))) + (C * (Math.pow(hex, --n)));
        System.out.println("AC DC in the decimal system: " +dec);
        // Second option
        int d = 0xACDC;
        System.out.println(d);
        Human human = new Human();
        human.said();
        Pidor pidor = new Pidor();
        pidor.said();
        Grisha grisha = new Grisha();
        grisha.said();


    }
}

class Human{
    void said(){
        System.out.println("Vhhh");
    }
 }

 class Pidor extends Human{
    @Override
     void said(){
        System.out.println("Huy");
    }
 }

class Grisha extends Human{
    @Override
    void said(){
        System.out.println("Poshol nahuy");
    }
}